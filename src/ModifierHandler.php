<?php

declare(strict_types=1);

namespace DKX\MonologModifierHandler;

use Monolog\Handler\AbstractHandler;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;

final class ModifierHandler extends AbstractHandler
{
	/** @var \Monolog\Handler\HandlerInterface */
	private $innerHandler;

	/** @var callable */
	private $modifier;

	public function __construct(HandlerInterface $innerHandler, callable $modifier, int $level = Logger::DEBUG, bool $bubble = true)
	{
		parent::__construct($level, $bubble);

		$this->innerHandler = $innerHandler;
		$this->modifier = $modifier;
	}

	public function handle(array $record): bool
	{
		$record = \call_user_func($this->modifier, $record);
		return $this->innerHandler->handle($record);
	}
}
