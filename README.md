# DKX\PHP\MonologModifierHandler

Modifier handler for monolog

## Installation

```bash
$ composer require dkx/monolog-modifier-handler
```

## Usage

```php
<?php

use DKX\MonologModifierHandler\ModifierHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$handler = new StreamHandler('php://stderr');

$logger = new Logger('default');
$logger->pushHandler(new ModifierHandler($handler, function (array $record) {
    return update_record_somehow($record);
}));
```
