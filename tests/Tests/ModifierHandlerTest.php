<?php

declare(strict_types=1);

namespace DKX\MonologModifierHandlerTests\Tests;

use DKX\MonologModifierHandler\ModifierHandler;
use Monolog\Handler\HandlerInterface;
use PHPUnit\Framework\TestCase;

final class ModifierHandlerTest extends TestCase
{
	public function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testModify(): void
	{
		/** @var \Monolog\Handler\HandlerInterface|\Mockery\MockInterface $innerHandler */
		$innerHandler = \Mockery::mock(HandlerInterface::class)
			->shouldReceive('handle')->andReturnUsing(function (array $record) {
				self::assertEquals([
					'message' => 'Lorem ipsum',
					'id' => 5,
				], $record);
				return true;
			})->getMock();

		$modifier = new ModifierHandler($innerHandler, function (array $record) {
			$record['id'] = 5;
			return $record;
		});

		$modifier->handle([
			'message' => 'Lorem ipsum',
		]);
	}
}
